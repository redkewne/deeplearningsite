from flask import Flask, request, render_template, redirect, url_for
from nn import NN


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/train', methods=['GET', 'POST'])
def train():
    r = request

    inputs = max(min(int(r.args.get('inputs', '1')), 6), 1)
    hidden = max(min(int(r.args.get('hidden', '0')), 4), 0)

    if r.method == 'GET':

        return render_template('train.html', DATA=[], WEIGHTS1=[], WEIGHTS2=[], inputs=inputs, hidden=hidden, hiddenvalues=[[0 for i in range(hidden)]], ACT='sigmoid')

    else:
        inlayer = [int(r.form.get(f'in{i}')) for i in range(inputs)]
        output = int(r.form.get('output'))
        activation = r.form.get('activation')
        learning_rate = max(min(float(r.args.get('lrate', '0.2')), 1.2), 0)
        iterations = max(min(int(r.form.get('iterations', '5')), 10), 1)

        training = NN.train(inlayer, output, hidden, learning_rate, activation, iterations)
        hiddenvalues = training[2]

        weights1 = training[1][0].flatten()
        weights2 = []

        if hidden > 0:
            weights2 = training[1][1].flatten()

        return render_template('train.html', DATA=training[0], WEIGHTS1=weights1, WEIGHTS2=weights2, inputs=inputs, hidden=hidden, hiddenvalues=hiddenvalues, ACT=activation)

if __name__ == '__main__':
    app.run(threaded=True, port=5000)