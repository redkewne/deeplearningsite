import numpy as np

class NN:

    def train(inl, outl, hidden, lr, activation, iterations):

        learnrate = lr

        inlayer = np.array([inl])
        output = outl

        activations = {'relu': (relu, relu_prime),
                       'sigmoid': (sigmoid, sigmoid_prime),
                       'leaky_relu': (leaky_relu, leaky_relu_prime),
                       'tanh': (tanh, tanh_prime),
                       'linear': (linear, linear_prime)}

        activ_func, activ_func_backprop = activations.get(activation, (sigmoid, sigmoid_prime))

        if hidden > 0:

            weights0 = 2*np.random.random((len(inl), hidden)) - 1
            weights1 = 2*np.random.random((hidden, 1)) - 1

            info = [f'Starting Weights0: {weights0}', f'Starting Weights1: {weights1}']

            for i in range(iterations):
                hidden = activ_func(np.dot(inlayer, weights0))
                prediction = activ_func(np.dot(hidden, weights1))
                error = (prediction - output) ** 2
                delta = prediction - output

                info.append(f'Error: {error[0]}, Delta: {delta[0]}, Prediction: {prediction[0]}')

                output_delta = delta * activ_func_backprop(np.dot(hidden, weights1))
                hidden_delta = output_delta.dot(weights1.T) * activ_func_backprop(np.dot(inlayer, weights0))

                weights1 = weights1 - hidden.T.dot(output_delta) * learnrate
                weights0 = weights0 - inlayer.T.dot(hidden_delta) * learnrate

            hidden = activ_func(np.dot(inlayer, weights0))
            prediction = activ_func(np.dot(hidden, weights1))

            error = (prediction - output) ** 2
            delta = prediction - output
            info.append(f'Error: {error[0]}, Delta: {delta[0]}, Prediction: {prediction[0]}')

            return (info, [weights0, weights1], hidden)

        else:
            weights0 = 2*np.random.random((len(inl), 1)) - 1
            info = [f'Starting Weights0: {weights0}']

            for i in range(iterations):

                prediction = activ_func(np.dot(inlayer, weights0))
                error = (prediction - output) ** 2
                delta = prediction - output
                info.append(f'Error: {error[0]}, Delta: {delta[0]}, Prediction: {prediction[0]}')

                inldelta = delta * activ_func_backprop(np.dot(inlayer, weights0))
                weights0 = weights0 - inlayer.T.dot(inldelta) * learnrate

            prediction = activ_func(np.dot(inlayer, weights0))
            error = (prediction - output) ** 2
            delta = prediction - output
            info.append(f'Error: {error[0]}, Delta: {delta[0]}, Prediction: {prediction[0]}')

        return (info, [weights0], [])

def relu(z):
    return np.maximum(0, z)

def relu_prime(z):
    return np.where(z>0, 1.0, 0.0)

def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
    return sigmoid(z)*(1-sigmoid(z))

def leaky_relu(z):
    return np.maximum(0.01*z, z)

def leaky_relu_prime(z):
    return np.where(z>=0, 1.0, 0.01)

def tanh(z):
    return (np.exp(z)-np.exp(-z))/(np.exp(z)+np.exp(-z))

def tanh_prime(z):
    return 1-tanh(z)**2

def linear(z):
    return z

def linear_prime(z):
    return 1
